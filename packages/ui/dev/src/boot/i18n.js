import { boot } from 'quasar/wrappers';
import { createI18n } from 'vue-i18n';

export default boot(({ app }) =>
{
  const i18n = createI18n({
    locale: 'de',
    fallbackLocale: 'en',
    silentTranslationWarn: true,
    warnHtmlInMessage: 'off'
  });

  // Set i18n instance on app
  app.use(i18n);
});
