import { h } from 'vue'
import { QBadge } from 'quasar'

export default {
  name: 'NaturalDate2',

  setup () {
    return () => h(QBadge, {
      class: 'NaturalDate2',
      label: 'NaturalDate2'
    })
  }
}
