import NaturalDate from './components/NaturalDate.vue'
import Component2 from './components/Component2'

const version = __UI_VERSION__

function install (app) {
  app.component(NaturalDate.name, NaturalDate)
  app.component(Component2.name, Component2)
}

export {
  version,
  NaturalDate,
  Component2,
  install
}
