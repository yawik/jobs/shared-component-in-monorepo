# shared-component-in-monorepo

This is a monorepo containing 2 default quasar apps and a commons project. Projects are organized by lerna.

The goal is to develop a Quasar component in the commons package that can be used in the App1 package and in the App2 package via import.

It should be possible to use a component developed under the packages/commons directory in the packages/app1 application.

Lerna enables the installation of the commons package via yarn.

example

```bash
cbleek@xenon:~/Projects/shared-component-in-monorepo$ cat packages/commons/package.json | grep name
  "name": "@yawik/commons",
cbleek@xenon:~/Projects/shared-component-in-monorepo/packages/app1$ yarn add @yawik/commons
yarn add v1.22.17
[1/5] Validating package.json...
[2/5] Resolving packages...
[3/5] Fetching packages...
[4/5] Linking dependencies...
warning " > @babel/eslint-parser@7.17.0" has unmet peer dependency "@babel/core@>=7.11.0".
warning " > eslint-webpack-plugin@2.6.0" has unmet peer dependency "webpack@^4.0.0 || ^5.0.0".
[5/5] Building fresh packages...
success Saved lockfile.
success Saved 1 new dependency.
info Direct dependencies
└─ @yawik/commons@0.0.1
info All dependencies
└─ @yawik/commons@0.0.1
```

## Install

```bash
git clone git@gitlab.com:yawik/jobs/shared-component-in-monorepo.git
cd shared-component-in-monorepo
yarn
yarn lerna bootstrap 
```
this installs lerna and dependencies of all packages. You can start the first apps with. 

```
yarn dev:app1
```

```
yarn dev:app2
```

